// Fill out your copyright notice in the Description page of Project Settings.


#include "Chunk.h"

#include "SimplexNoiseBPLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "KismetProceduralMeshLibrary.h"



// Sets default values
AChunk::AChunk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add root component
	RootComponent = CreateAbstractDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Add procedural mesh component 
	ChunkMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("PlanetMesh"));

	SplitPoints = {
		FVector(1, 1, 0),
        FVector(1, -1, 0),
        FVector(-1, 1, 0),
        FVector(-1, -1, 0)
    };

	NoiseAmplitude = 10.0f;
	NoiseAmount = 0.025f;
	Location = FVector::ZeroVector;
}

// Called when the game starts or when spawned
void AChunk::BeginPlay()
{
	Super::BeginPlay();

	for (FVector& Point : SplitPoints)
	{
		Point *= PlanetNodeScale * 0.5f;
	}
	
	DistFromPlanetNode = PlanetNodeScale * LODDistanceMultiplier;
	bPendingUpdate = true;

	HandleSubdivision();
	MapPlaneToSphere();
	(new FAutoDeleteAsyncTask <NoiseThreadTask>(this))->StartBackgroundTask();
	ChunkMesh->SetMaterial(0, MaterialInterface);
}

// Called every frame
void AChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckPlayerInBounds();
	CheckBounds();
}

void AChunk::Split()
{
	for (FVector Point : SplitPoints)
	{
		CreateChild(Point);
	}

	ChunkMesh->SetMeshSectionVisible(0, false);
	ChunkMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AChunk::CreateChild(FVector OffsetLocation)
{
	auto ChunkNode = Cast<AChunk>(UGameplayStatics::BeginSpawningActorFromClass(this, PlanetNodeClass, GetActorTransform()));
	ChunkNode->ParentChunkRef 			= this;
	ChunkNode->MaxSplits 				= MaxSplits;
	ChunkNode->NodeSplitCount 			= NodeSplitCount + 1;
	ChunkNode->Subdivision 				= Subdivision;
	ChunkNode->PlanetLocation 			= PlanetLocation;
	ChunkNode->PlanetScale 				= PlanetScale;
	ChunkNode->PlanetNodeScale 			= PlanetNodeScale * 0.5f;
	ChunkNode->LODDistanceMultiplier 	= LODDistanceMultiplier;
	ChunkNode->MaterialInterface		= MaterialInterface;
	ChunkNode->NoiseAmplitude			= NoiseAmplitude;
	ChunkNode->NoiseAmount				= NoiseAmount;
	ChunkNode->Location					= Location + OffsetLocation;

	UGameplayStatics::FinishSpawningActor(ChunkNode, GetActorTransform());
}

void AChunk::CheckPlayerInBounds()
{
	if (APawn* Pawn = UGameplayStatics::GetPlayerPawn(this, 0))
	{
		FVector PlayerPos = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
		if (FVector(PlayerPos - ChunkCenter).Size() < DistFromPlanetNode)
		{
			bInBounds = true;
		}
		else
		{
			if (FVector(PlayerPos - ChunkCenter).Size() > DistFromPlanetNode * 1.5f)
			{
				bInBounds = false;
			}
		}
	}
}

void AChunk::CheckBounds()
{
	if (bInBounds)
	{
		if (NodeSplitCount < MaxSplits)
		{
			if (bPendingUpdate)
			{
				Split();
				bPendingUpdate = false;
			}
		}
	}
	else
	{
		bPendingUpdate = true;
	}

	if (IsValid(ParentChunkRef) && !ParentChunkRef->bInBounds)
	{
		ChunkMesh->SetMeshSectionVisible(0, false);
		ChunkMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ParentChunkRef->ChunkMesh->SetMeshSectionVisible(0, true);
		Destroy();
	}
}

void AChunk::Subdivide(int32 a, int32 b, int32 c)
{
	FVector Va, Vb, Vc, Vab, Vbc, Vca;

	Va = Vertices[a];
	Vb = Vertices[b];
	Vc = Vertices[c];
	Vab = FMath::Lerp(Va, Vb, 0.5);
	Vbc = FMath::Lerp(Vb, Vc, 0.5);
	Vca = FMath::Lerp(Vc, Va, 0.5);
	
	Vertices_New.Add(Va);
	Vertices_New.Add(Vab);
	Vertices_New.Add(Vca);

	Vertices_New.Add(Vca); 
	Vertices_New.Add(Vbc);
	Vertices_New.Add(Vc);

	Vertices_New.Add(Vab);
	Vertices_New.Add(Vb);
	Vertices_New.Add(Vbc);

	Vertices_New.Add(Vab);
	Vertices_New.Add(Vbc); 
	Vertices_New.Add(Vca); 

	Index += 3;
}

void AChunk::HandleSubdivision()
{
	Vertices = {
		FVector(-1,	-1,	0),
	    FVector(-1,	1,	0),
	    FVector(1,	-1,	0),
	    FVector(1,	1,	0)
	};

	FVector Avg;
	for (FVector& Vertex : Vertices)
	{
		Vertex *= PlanetNodeScale;
		Vertex += Location;
		Avg += Vertex;
	}
	Avg.Normalize();
	Avg *= PlanetScale;
	Avg = GetActorRotation().Quaternion() * Avg;
	ChunkCenter = GetActorLocation() + Avg;
	
	Triangles = {
		1	,	2	,	0	,
        1	,	3	,	2	};
	
	// Handle the subdivisions
	for (int i = 0; i < Subdivision; i++)
	{
		for (int j = 0; j < Triangles.Num() / 3; j++)
		{
			Subdivide(Triangles[Index], Triangles[Index + 1], Triangles[Index + 2]);
		}

		// Empty
		Vertices.Empty();
		Triangles.Empty();

		//Assign new to current
		Vertices = Vertices_New;

		//New empty 
		Vertices_New.Empty();

		//Build tri indices
		BuildTriangleList();

		//Reset index counters 
		Index = 0;
	}
	
	ChunkMesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UV0, VertexColors, Tangents, true);
	ChunkMesh->SetWorldLocation(GetActorLocation());
	ChunkMesh->SetWorldRotation(GetActorRotation());
}

void AChunk::BuildTriangleList()
{
	VertexColors.Init(FColor(255, 255, 255, 0), Vertices.Num());
	for (int i = 0; i < Vertices.Num(); i++)
	{
		// Build vertex index list
		Triangles.Add(i);
	}
	ChunkMesh->UpdateMeshSection(0, Vertices, Normals, UV0, UpVertexColors, Tangents);
}

void AChunk::CalculateNormals()
{
	int32 Ni = 0;

	Normals.Empty();
	for (int32 i = 0; i < Triangles.Num() / 3; i++)
	{
		FVector a = Vertices[Triangles[Ni]];
		FVector b = Vertices[Triangles[Ni + 1]];
		FVector c = Vertices[Triangles[Ni + 2]];

		FVector n = FVector::CrossProduct(a - b, c - a);
		
		a.Normalize();
		b.Normalize();
		c.Normalize();
		
		Normals.Add(a);
		Normals.Add(b);
		Normals.Add(c);
		Ni += 3;
	}
	
	ChunkMesh->UpdateMeshSection(0, Vertices, Normals, UV0, UpVertexColors, Tangents);
}

void AChunk::MapPlaneToSphere()
{
	// Map cube to sphere
	for (int i = 0; i < Vertices.Num(); i++)
	{
		Vertices[i].Normalize();
		Vertices[i] *= PlanetScale;
	}
}

void AChunk::ProcessVertices()
{
	for (FVector& Vertex : Vertices)
	{
		const FVector WorldPoint = (GetActorLocation() + GetActorRotation().Quaternion() * Vertex) * NoiseAmount;
		const float v = USimplexNoiseBPLibrary::SimplexNoise3D(
            WorldPoint.X,
            WorldPoint.Y,
            WorldPoint.Z) * NoiseAmplitude;
		
		FVector Dir = Vertex;
		Dir.Normalize();
		Vertex = Vertex + Dir * v;
	}
}


NoiseThreadTask::NoiseThreadTask(AChunk* InChunkRef)
	: ChunkRef(InChunkRef)
{ }

NoiseThreadTask::~NoiseThreadTask()
{
	UE_LOG(LogTemp, Warning, TEXT("Task Finished"));
}

void NoiseThreadTask::DoWork()
{
	ChunkRef->ProcessVertices();
	ChunkRef->CalculateNormals();
}
void NoiseThreadTask::DoWorkMain()
{
	DoWork();
}