// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TestPlanetGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TESTPLANET_API ATestPlanetGameMode : public AGameMode
{
	GENERATED_BODY()
	
};
