// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Chunk.generated.h"

UCLASS()
class TESTPLANET_API AChunk : public AActor
{
	GENERATED_BODY()
	

public:	
	AChunk();

	
public:
	// Create our procedural mesh component
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    UProceduralMeshComponent* ChunkMesh;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Defaults")
    TSubclassOf<AChunk> PlanetNodeClass;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Defaults")
	int32 Subdivision;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Defaults")
	int32 MaxSplits;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float LODDistanceMultiplier;
    
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float NoiseAmount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float NoiseAmplitude;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float PlanetNodeScale;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    UMaterialInterface* MaterialInterface;


public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void Split();
	void CreateChild(FVector OffsetLocation);
	void CheckBounds();
	void CheckPlayerInBounds();

	void Subdivide(int32 a, int32 b, int32 c);
	void HandleSubdivision();
	void ProcessVertices();
	void BuildTriangleList();
	void CalculateNormals();
	void MapPlaneToSphere();

	
public:
	FVector PlanetLocation;
	float PlanetScale;
	FVector Location;
	inline TArray<FVector>& GetVertices() { return Vertices; }
	
private:
	int32 NodeSplitCount;
	TArray<FVector> SplitPoints;
	FVector ChunkCenter;
	
	AChunk* ParentChunkRef;

	bool bInBounds;
	bool bPendingUpdate;
	float DistFromPlanetNode;
	
	TArray<FVector> Vertices;
	TArray<FLinearColor> VertexColors;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UV0;
	TArray<FColor> UpVertexColors;
	TArray<FProcMeshTangent> Tangents;

	TArray<FVector> Vertices_New;
	int32 Index;
};


class NoiseThreadTask : public FNonAbandonableTask
{
public:
    NoiseThreadTask(AChunk* InChunkRef);

	~NoiseThreadTask();

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(NoiseThreadTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	AChunk* ChunkRef;

	void DoWorkMain();
	void DoWork();
};