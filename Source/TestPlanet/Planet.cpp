// Fill out your copyright notice in the Description page of Project Settings.


#include "Planet.h"
#include "Kismet/GameplayStatics.h"
#include "Chunk.h"
#include "SimplexNoiseBPLibrary.h"
#include "PlanetCharacter.h"


APlanet::APlanet()
{

}

void APlanet::BeginPlay()
{
	Super::BeginPlay();

	TArray<FTransform> ChunkTransforms = {
		FTransform(FRotator::MakeFromEuler(FVector(0, 0, 0)), GetActorLocation()),
		FTransform(FRotator::MakeFromEuler(FVector(180, 0, 0)), GetActorLocation()),
		FTransform(FRotator::MakeFromEuler(FVector(0, -90, 0)), GetActorLocation()),
		FTransform(FRotator::MakeFromEuler(FVector(0, 90, 0)), GetActorLocation()),
		FTransform(FRotator::MakeFromEuler(FVector(90, 0, 0)), GetActorLocation()),
		FTransform(FRotator::MakeFromEuler(FVector(-90, 0, 0)), GetActorLocation()),
	};

	for (FTransform ChunkTransform : ChunkTransforms)
	{
		CreateChunk(ChunkTransform);
	}
}

void APlanet::CreateChunk(FTransform pointTransform)
{
	auto ChunkNode = Cast<AChunk>(UGameplayStatics::BeginSpawningActorFromClass(this, PlanetNodeClass, pointTransform));
	ChunkNode->PlanetLocation = GetActorLocation();
	ChunkNode->PlanetScale = PlanetScale;
	ChunkNode->PlanetNodeScale = PlanetScale;
	ChunkNode->Location.Z = PlanetScale;
	ChunkNode->NoiseAmount = NoiseAmount;
	ChunkNode->NoiseAmplitude = NoiseAmplitude;
	UGameplayStatics::FinishSpawningActor(ChunkNode, pointTransform);

	ChunkRefs.Add(ChunkNode);
}

FVector APlanet::GetRandomPointOnPlanet()
{
	FVector UnitCirclePoint = FVector(
		FMath::Cos(FMath::RandRange(0.0f, PI * 2.0f)),
		FMath::Cos(FMath::RandRange(0.0f, PI * 2.0f)),
		FMath::Cos(FMath::RandRange(0.0f, PI * 2.0f)));
	UnitCirclePoint.Normalize();

	const FVector SurfacePoint = GetActorLocation() + UnitCirclePoint * PlanetScale;
	const float V = USimplexNoiseBPLibrary::SimplexNoise3D(
        SurfacePoint.X * NoiseAmount,
        SurfacePoint.Y * NoiseAmount,
        SurfacePoint.Z * NoiseAmount) * NoiseAmplitude;

	return SurfacePoint + UnitCirclePoint * V;
}