// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectionComponent.h"

#include "PlanetCharacter.h"


// Sets default values for this component's properties
UCollectionComponent::UCollectionComponent()
{
	Collected = 0;
	NeedToCollect = 200;
}

void UCollectionComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UCollectionComponent::AddCollected()
{
	Collected++;

	if (Collected == NeedToCollect)
	{
		Collected = 0;
		OnCollectedEnough();
	}
}

void UCollectionComponent::OnCollectedEnough()
{
	if(auto Player = Cast<APlanetCharacter>(GetOwner()))
	{
		Player->RandomizeColor();
	}
}
