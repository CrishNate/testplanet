// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GravityCharacter.h"
#include "GameFramework/SpringArmComponent.h"

#include "PlanetCharacter.generated.h"


UCLASS()
class TESTPLANET_API APlanetCharacter : public AGravityCharacter
{
	GENERATED_BODY()

public:
	APlanetCharacter(const FObjectInitializer& ObjectInitializer);

	
protected:
	virtual void BeginPlay() override;

	
public:	
	UFUNCTION()
    void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void InputMoveForward(float Value);
	void InputMoveRight(float Value);
	void InputTurn(float Value);
	void InputLookUp(float Value);

	void Pickup(class ACollectable* Collectable);
	void RandomizeColor();

	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Default")
	AActor* PlanetRef;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default")
	class UCollectionComponent* CollectionComponent;

	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly)
    UCapsuleComponent* CapsuleComponentOverlap;
	
	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly)
    USpringArmComponent* SpringArmComponent;
	

private:
	UMaterialInstanceDynamic* MaterialInstanceDynamic;
};
