// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemPlanet.h"

#include "Engine/World.h"
#include "Items/Collectable.h"


AItemPlanet::AItemPlanet()
    : ItemCount(200)
{
}

void AItemPlanet::BeginPlay()
{
    Super::BeginPlay();

    GenerateItems();
}

void AItemPlanet::GenerateItems()
{
    for (int32 i = 0; i < ItemCount; i++)
    {
        FVector Point = GetRandomPointOnPlanet();
        FVector DirFromSurface = (Point - GetActorLocation());
        DirFromSurface.Normalize();
        FRotator Rotator = DirFromSurface.ToOrientationRotator();
        
        GetWorld()->SpawnActor<ACollectable>(ItemClass, FTransform(Rotator, Point + DirFromSurface * OffsetItemSpawn));
    }
}