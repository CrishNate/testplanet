// Fill out your copyright notice in the Description page of Project Settings.


#include "Collectable.h"


ACollectable::ACollectable()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ACollectable::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACollectable::Collected(ACharacter* InstigateBy)
{
	OnCollected(InstigateBy);
	Destroy();
}

