// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"

#include "Collectable.generated.h"


UCLASS()
class TESTPLANET_API ACollectable : public AActor
{
	GENERATED_BODY()
	
public:	
	ACollectable();


protected:
	virtual void BeginPlay() override;


public:	
	virtual void Collected(ACharacter* InstigateBy);

	UFUNCTION(BlueprintImplementableEvent)
	void OnCollected(ACharacter* InstigateBy);
};
