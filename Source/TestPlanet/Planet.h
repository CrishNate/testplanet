// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Planet.generated.h"


UCLASS()
class TESTPLANET_API APlanet : public AActor
{
	GENERATED_BODY()
	
	
public:	
	APlanet();


public:
	virtual void BeginPlay() override;
	
	void CreateChunk(FTransform pointTransform);

	FVector GetRandomPointOnPlanet();


public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Defaults")
	TSubclassOf<class AChunk> PlanetNodeClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float PlanetScale;
    
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float NoiseAmount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
    float NoiseAmplitude;

	
private:
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<AChunk*> ChunkRefs;

};
