// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CollectionComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTPLANET_API UCollectionComponent : public UActorComponent
{
	GENERATED_BODY()


public:	
	UCollectionComponent();


public:
	void AddCollected();
	void OnCollectedEnough();
	
	
protected:
	virtual void BeginPlay() override;


public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default")
	int32 NeedToCollect;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default")
	int32 Collected;
};
