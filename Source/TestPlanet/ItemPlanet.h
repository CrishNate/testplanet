// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Planet.h"
#include "UObject/ObjectMacros.h"

#include "ItemPlanet.generated.h"


UCLASS()
class TESTPLANET_API AItemPlanet : public APlanet
{
	GENERATED_BODY()


public:
    AItemPlanet();
    
    
public:
    virtual void BeginPlay() override;

    void GenerateItems();


public:
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default")
    int32 ItemCount;
    
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default")
    TSubclassOf<class ACollectable> ItemClass;

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Default")
    float OffsetItemSpawn;
};
