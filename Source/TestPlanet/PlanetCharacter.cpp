// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetCharacter.h"

#include "GravityMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Planet.h"
#include "Kismet/KismetMathLibrary.h"
#include "Items/Collectable.h"
#include "CollectionComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"


APlanetCharacter::APlanetCharacter(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
	CollectionComponent = CreateDefaultSubobject<UCollectionComponent>(TEXT("CollectionComponent"));
	
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("StaticMeshComponent"));
	SpringArmComponent->SetupAttachment(GetMesh());
	
	CapsuleComponentOverlap = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponentOverlap"));
	CapsuleComponentOverlap->SetupAttachment(GetCapsuleComponent());
	CapsuleComponentOverlap->OnComponentBeginOverlap.AddDynamic(this, &APlanetCharacter::OnBeginOverlap);
}

// Called when the game starts or when spawned
void APlanetCharacter::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Out;
	UGameplayStatics::GetAllActorsOfClass(this, APlanet::StaticClass(),  Out);

	PlanetRef = Out[0];

	MaterialInstanceDynamic = UMaterialInstanceDynamic::Create(GetMesh()->GetMaterial(0), this);
	GetMesh()->SetMaterial(0, MaterialInstanceDynamic);
}

// Called every frame
void APlanetCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(PlanetRef))
	{
		FVector PlanetDir = (PlanetRef->GetActorLocation() - GetActorLocation());
		PlanetDir.Normalize();
	
		Cast<UGravityMovementComponent>(GetMovementComponent())->SetGravityDirection(PlanetDir);
	}
}

// Called to bind functionality to input
void APlanetCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlanetCharacter::InputMoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlanetCharacter::InputMoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APlanetCharacter::InputTurn);
	PlayerInputComponent->BindAxis("LookUp", this, &APlanetCharacter::InputLookUp);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlanetCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &APlanetCharacter::StopJumping);
}

void APlanetCharacter::InputTurn(float Value)
{
	AddActorLocalRotation(FRotator::MakeFromEuler(FVector(0, 0, Value)));
}

void APlanetCharacter::InputLookUp(float Value)
{
	SpringArmComponent->AddRelativeRotation(FRotator::MakeFromEuler(FVector(0, Value, 0)));
}

void APlanetCharacter::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (auto Item = Cast<ACollectable>(OtherActor))
	{
		CollectionComponent->AddCollected();
		Item->Collected(this);
	}
}

void APlanetCharacter::Pickup(ACollectable* Collectable)
{
	Collectable->Destroy();
}

void APlanetCharacter::RandomizeColor()
{
	MaterialInstanceDynamic->SetVectorParameterValue("Color", FLinearColor::MakeFromHSV8(FMath::RandRange(0, 255), 255, 255));
}

void APlanetCharacter::InputMoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector(), Value);
}

void APlanetCharacter::InputMoveRight(float Value)
{
	AddMovementInput(GetActorRightVector(), Value);
}
